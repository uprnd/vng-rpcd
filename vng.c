#define _GNU_SOURCE /* crypt() */

#include <sys/types.h>
#include <dirent.h>
#include <libubus.h>
#include <iwinfo.h>
#include <iwinfo/utils.h>

#include <rpcd/plugin.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

static struct blob_buf buf;

static struct uci_context *cursor;
static const struct iwinfo_ops *iw;
static const char *ifname;

static void
rpc_iwinfo_close(void) {
    iw = NULL;
    ifname = NULL;
    iwinfo_finish();
}

static void
rpc_iwinfo_call_str(struct blob_buf *buff, const char *name, int (*func)(const char *, char *)) {
    char rv[IWINFO_BUFSIZE] = {0};

    if (!func(ifname, rv))
	blobmsg_add_string(buff, name, rv);
}

static int rpc_iwinfo_devices(struct blob_buf *buf) {

    struct dirent *e;
    DIR *d;
    void *c1, *c2, *c3, *c4, *c5;
    int i = 0, len = 0;
    char mac[18];
    char res[IWINFO_BUFSIZE];
    struct iwinfo_assoclist_entry *a;

    d = opendir("/sys/class/net");
    if (!d) return UBUS_STATUS_UNKNOWN_ERROR;

    c1 = blobmsg_open_array(buf, NULL);

    while ((e = readdir(d)) != NULL) {
	if (e->d_type != DT_LNK)
	    continue;

	if (iwinfo_type(e->d_name)) {
	    c2 = blobmsg_open_table(buf, NULL);

	    /* get device */
	    ifname = e->d_name;
	    blobmsg_add_string(buf, "device", ifname);

	    /* get iw info */
	    iw = iwinfo_backend(ifname);
	    if (!iw) continue;

	    /* get ssid */
	    rpc_iwinfo_call_str(buf, "ssid", iw->ssid);

	    c3 = blobmsg_open_array(buf, "host");
	    if (!iw->assoclist(ifname, res, &len) && (len > 0)) {
		for (i = 0; i < len; i += sizeof (struct iwinfo_assoclist_entry)) {
		    a = (struct iwinfo_assoclist_entry *) &res[i];

		    snprintf(mac, sizeof (mac), "%02X:%02X:%02X:%02X:%02X:%02X",
			    a->mac[0], a->mac[1], a->mac[2],
			    a->mac[3], a->mac[4], a->mac[5]);
		    c4 = blobmsg_open_table(buf, NULL);

		    blobmsg_add_string(buf, "mac", mac);
		    blobmsg_add_u32(buf, "signal", a->signal);
		    blobmsg_add_u32(buf, "noise", a->noise);
		    blobmsg_add_u32(buf, "inactive", a->inactive);

		    c5 = blobmsg_open_table(buf, "rx");
		    blobmsg_add_u32(buf, "rate", a->rx_rate.rate);
		    blobmsg_add_u32(buf, "mcs", a->rx_rate.mcs);
		    blobmsg_add_u8(buf, "40mhz", a->rx_rate.is_40mhz);
		    blobmsg_add_u8(buf, "short_gi", a->rx_rate.is_short_gi);
		    blobmsg_close_table(buf, c5);

		    c5 = blobmsg_open_table(buf, "tx");
		    blobmsg_add_u32(buf, "rate", a->tx_rate.rate);
		    blobmsg_add_u32(buf, "mcs", a->tx_rate.mcs);
		    blobmsg_add_u8(buf, "40mhz", a->tx_rate.is_40mhz);
		    blobmsg_add_u8(buf, "short_gi", a->tx_rate.is_short_gi);
		    blobmsg_close_table(buf, c5);

		    blobmsg_close_array(buf, c4);

		}
	    }
	    blobmsg_close_array(buf, c3);

	    blobmsg_close_table(buf, c2);
	}
    }

    blobmsg_close_array(buf, c1);
    closedir(d);

    rpc_iwinfo_close();
    return UBUS_STATUS_OK;
}

static FILE *
dnsmasq_leasefile(void) {
    FILE *leases = NULL;
    struct uci_package *p;
    struct uci_element *e;
    struct uci_section *s;
    struct uci_ptr ptr = {
	.package = "dhcp",
	.section = NULL,
	.option = "leasefile"
    };

    uci_load(cursor, ptr.package, &p);

    if (!p)
	return NULL;

    uci_foreach_element(&p->sections, e) {
	s = uci_to_section(e);

	if (strcmp(s->type, "dnsmasq"))
	    continue;

	ptr.section = e->name;
	uci_lookup_ptr(cursor, &ptr, NULL, true);
	break;
    }

    if (ptr.o && ptr.o->type == UCI_TYPE_STRING)
	leases = fopen(ptr.o->v.string, "r");

    uci_unload(cursor, p);

    return leases;
}

static int
rpc_luci2_network_leases(struct blob_buf *dhcp_buf) {

    FILE *leases;
    void *c, *d;
    char line[128];
    char *ts, *mac, *addr, *name;
    time_t now = time(NULL);

    c = blobmsg_open_array(dhcp_buf, "leases");

    leases = dnsmasq_leasefile();

    if (!leases)
	goto out;


    while (fgets(line, sizeof (line) - 1, leases)) {
	ts = strtok(line, " \t");
	mac = strtok(NULL, " \t");
	addr = strtok(NULL, " \t");
	name = strtok(NULL, " \t");

	if (!ts || !mac || !addr || !name)
	    continue;

	if (strchr(addr, ':'))
	    continue;

	d = blobmsg_open_table(dhcp_buf, NULL);

	blobmsg_add_u32(dhcp_buf, "expires", atoi(ts) - now);
	blobmsg_add_string(dhcp_buf, "macaddr", mac);
	blobmsg_add_string(dhcp_buf, "ipaddr", addr);

	if (strcmp(name, "*"))
	    blobmsg_add_string(dhcp_buf, "hostname", name);

	blobmsg_close_table(dhcp_buf, d);
    }

    fclose(leases);

out:
    blobmsg_close_array(dhcp_buf, c);
    return 0;
}

static int
rpc_vng_iwinfo_dhcp(struct blob_buf *info_buf, struct blob_buf *dhcp_buf) {

    struct blob_attr *info_head, *dhcp_head;
    struct blob_attr *info_devices, *info_device, *dhcp_devices, *dhcp_device;
    struct blob_attr *info_attrs, *info_attr;

    int rem1, rem2, rem3, rem4, rem5, rem6, rem7, rem8;
    void *c1, *c2, *c3, *c4;
    char *info_mac;

    c1 = blobmsg_open_array(&buf, "data");

    blobmsg_for_each_attr(info_head, info_buf->head, rem1) {

	blobmsg_for_each_attr(info_devices, info_head, rem2) {
	    c2 = blobmsg_open_table(&buf, NULL);

	    blobmsg_for_each_attr(info_device, info_devices, rem3) {

		if (blob_id(info_device) != BLOBMSG_TYPE_ARRAY ||
			strcmp("host", blobmsg_name(info_device))) {
		    blobmsg_add_blob(&buf, info_device);

		} else {

		    c3 = blobmsg_open_array(&buf, "host");

		    blobmsg_for_each_attr(info_attrs, info_device, rem4) {
			c4 = blobmsg_open_table(&buf, NULL);

			blobmsg_for_each_attr(info_attr, info_attrs, rem5) {

			    if (blob_id(info_attr) == BLOBMSG_TYPE_STRING &&
				    !strcmp("mac", blobmsg_name(info_attr))) {

				info_mac = blobmsg_data(info_attr);

				blobmsg_for_each_attr(dhcp_head, dhcp_buf->head, rem6) {

				    blobmsg_for_each_attr(dhcp_devices, dhcp_head, rem7) {
					bool is_mac = false;

					blobmsg_for_each_attr(dhcp_device, dhcp_devices, rem8) {

					    if (!strcmp("macaddr", blobmsg_name(dhcp_device))) {
						if (!strcasecmp(blobmsg_data(dhcp_device), info_mac)) {
						    is_mac = true;
						} else {
						    is_mac = false;
						}

					    } else {
						if (is_mac) {
						    blobmsg_add_blob(&buf, dhcp_device);
						}
					    }

					}
				    }

				}

			    } else {

			    }

			    blobmsg_add_blob(&buf, info_attr);
			}
			blobmsg_close_table(&buf, c4);
		    }
		    blobmsg_close_array(&buf, c3);
		}
	    }

	    blobmsg_close_table(&buf, c2);
	}
	blobmsg_close_array(&buf, c1);
    }

    return UBUS_STATUS_OK;
}

static int
rpc_vng_iwinfo(struct ubus_context *ctx, struct ubus_object *obj,
	struct ubus_request_data *req, const char *method,
	struct blob_attr * msg) {

    blob_buf_init(&buf, 0);
    int ret = UBUS_STATUS_OK;

    //    /* get list  devices */
    struct blob_buf info_buf = {0};
    blob_buf_init(&info_buf, 0);
    ret = rpc_iwinfo_devices(&info_buf);
    if (ret != UBUS_STATUS_OK) {
	goto error;
    }

    /* get list dhcp lease*/
    struct blob_buf dhcp_buf = {0};
    blob_buf_init(&dhcp_buf, 0);
    rpc_luci2_network_leases(&dhcp_buf);


    /* get list entry */
    rpc_vng_iwinfo_dhcp(&info_buf, &dhcp_buf);
    ubus_send_reply(ctx, req, buf.head);

error:
    blob_buf_free(&info_buf);
    blob_buf_free(&dhcp_buf);

    return ret;
}

static int
rpc_vng_api_init(const struct rpc_daemon_ops *o, struct ubus_context * ctx) {

    static const struct ubus_method vng_methods[] = {
	UBUS_METHOD_NOARG("iwinfo", rpc_vng_iwinfo)
    };

    static struct ubus_object_type vng_type =
	    UBUS_OBJECT_TYPE("luci-rpc-vng", vng_methods);

    static struct ubus_object vng_obj = {
	.name = "vng",
	.type = &vng_type,
	.methods = vng_methods,
	.n_methods = ARRAY_SIZE(vng_methods),
    };

    cursor = uci_alloc_context();

    if (!cursor)
	return UBUS_STATUS_UNKNOWN_ERROR;

    return ubus_add_object(ctx, &vng_obj);
}


struct rpc_plugin rpc_plugin = {
    .init = rpc_vng_api_init
};




